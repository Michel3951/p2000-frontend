import { boot } from 'quasar/wrappers'
import Echo from 'laravel-echo'
import * as Pusher from 'pusher-js'

window.Pusher = Pusher;

const echo = new Echo({
  broadcaster: 'pusher',
  key: 'fDSI3RdOoFKxV5ScKA0DzUqeOVdG9WLd',
  wsHost: process.env.BACKEND_URL,
  wsPort: 6001,
  forceTLS: true,
  disableStats: true,
});

window.Echo = echo

export default boot(({ app }) => {
  app.config.globalProperties.$echo = echo
})
