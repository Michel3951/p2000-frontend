import { boot } from 'quasar/wrappers'
import moment from 'moment-timezone'

export default boot(({ app }) => {

  app.config.globalProperties.$moment = moment
})
